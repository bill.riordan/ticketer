# Ticketing Service

Hate Ticketmaster? Well do we have a service for you! 
We supply web-based application for users to sign up and purchase tickets for your events.
All you need is a scanner; we do all the rest!

## Installation (developer stuff)

1. clone the repository
2. run `composer install`
3. run `composer dump-autoload`
4. to set up db locally:
    - run `php artisan migrate`
    - optionally, if you'd like to seed the db: run `php artisan db:seed`
    - after seeding, to see qr codes you will need to run `php artisan image:create`
5. run `php artisan serve` to interact with the application at (usually) localhost:8000

You're good to go!
