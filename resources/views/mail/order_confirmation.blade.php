<h2>Hey, {{ $order->customer->actionable->getName() }}</h2>
<p>Your order for {{ $order->tickets->count() }} ticket(s) has been placed!</p>
<p>{{ $order->event->attendable->name }} is at <a href="#">{{ $order->event->venue->name }}</a> on {{ $order->event->created_at->toDayDateTimeString() }}
@foreach($order->tickets as $ticket)
	{!! $ticket->ticketable->render() !!}
	<br>
	<hr>
	<br>
@endforeach