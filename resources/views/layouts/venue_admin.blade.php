<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Dashboard</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="{{ asset('css/backstage.css') }}" rel="stylesheet">

</head>
	<nav class="navbar navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">{{ $venue->name }}</a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            </div>
        </div>
    </nav>
    <body id="app-layout">
    	<div class="container-fluid">
    		<div class="row">
                <div class="col-sm-0 col-lg-1 sidebar"></div>
                <div class="col-sm-12 col-lg-10 foreground">@yield('content')</div>
                <div class="col-sm-0 col-lg-1 sidebar"></div>
        	</div>
            <div class="row">
            </div>
        </div>
    </body>
</html>
