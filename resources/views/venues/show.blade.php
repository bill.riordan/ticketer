@extends('layouts.venue_admin')
@section('content')
<br>
@include('partials.stats.ticket_sale_totals', 
[	'totalRevenue' => $totalRevenue, 
	'totalTickets' => $totalTickets,
	'totalEvents' => $venue->events->count(),
])

<div class="row">
	<div class="col-md-11"><h2>Upcoming Events</h2></div>
	<div class="col-md-1"><a href="/events/add"><button type="button" class="btn btn-primary">Add Event</button></a></div>
	@include('partials.lists.events', ['events' => $futureEvents])
</div>

<div class="row">
	<div class="col-md-2"><h2>Past Events</h2></div>
	@include('partials.lists.events', ['events' => $pastEvents])
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
@endsection