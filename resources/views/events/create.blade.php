@extends('layouts.venue_admin')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Create Event</div>
				<div class="panel-body">
				@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ route('events.store', ['$subdomain' => $subdomain]) }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">On Sale Date</label>
							<div class="col-md-6">
								<input type="date" class="form-control" name="on_sale_date" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Start Time</label>
							<div class="col-md-6">
								<input type="date" class="form-control" name="start_date" value="{{ \Carbon\Carbon::parse($event->start_date)->toDateString() }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">General Admission Cost:</label>
							<div class="col-md-6">
								$ <input name="general_admission_cost" type="number" min="0.01" step="0.01" max="2500" value="{{ old('phone') }}"/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Event Type: </label>
							{{ Form::select('attendable_type', $attendable_types, $event->attendable_type) }}
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Hardware: </label>
							{{ Form::select('ticketable_type', $ticketable_types) }}
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Description</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="description" value="{{ $event->attendable->description }}">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Create</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
