<table class="table events-list">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Type</th>
      <th scope="col">Hardware</th>
      <th scope="col">GA Cost</th>
      <th scope="col">Capacity</th>
      <th scope="col">Tickets Sold</th>
      <th scope="col">Total Revenue</th>
      <th scope="col">On Sale Date</th>
      <th scope="col">Start Date</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
  	@foreach($events as $i => $event)
	<tr>
      <td>{{ $event->attendable->name }}</td>
      <td>{{ $event->attendable_type }}</td>
      <td>{{ $event->ticketable_type }}</td>
      <td>${{ sprintf('%01.2f', $event->general_admission_cost / 100) }}</td>
      <td>&#8734;</td>
      <td>{{ $event->tickets->count() }}</td>
      <td>${{ sprintf('%01.2f', $event->general_admission_cost * $event->tickets->count() / 100) }}</td>
      <td>{{ \Carbon\Carbon::parse($event->on_sale_date)->toDayDateTimeString() }}</td>
      <td>{{ \Carbon\Carbon::parse($event->start_date)->toDayDateTimeString() }}</td>
      <td>
        @if(\Carbon\Carbon::parse($event->start_date)->gt(\Carbon\Carbon::now()))
        <a href="/events/{{ $event->id }}/edit"><button type="button" class="btn btn-outline-primary">Edit</button></a>
        @endif
        @if(\Carbon\Carbon::parse($event->on_sale_date)->gt(\Carbon\Carbon::now()))&nbsp;   
        <a href="/events/{{ $event->id }}/delete"><button type="button" class="btn btn-outline-secondary">Delete</button></a>
        @endif
      </td>
    </tr>
	@endforeach
  </tbody>
</table>