<div class="row">

	<!-- total events -->
	<div class="col-md-2">
		<div class="card">
		  <div class="card-body">
		    <h5 class="card-title">Events Held</h5>
		    {{ $totalEvents }}
		  </div>
		</div>
	</div>

	<!-- total tickets sold -->
	<div class="col-md-2">
		<div class="card">
		  <div class="card-body">
		    <h5 class="card-title">Tickets Sold To Date</h5>
		    {{ $totalTickets }}
		  </div>
		</div>
	</div>

	<!-- total revenue -->
	<div class="col-md-2">
		<div class="card">
		  <div class="card-body">
		    <h5 class="card-title">Revenue To Date</h5>
		    ${{ sprintf('%01.2f', $totalRevenue / 100) }}
		  </div>
		</div>
	</div>

</div>