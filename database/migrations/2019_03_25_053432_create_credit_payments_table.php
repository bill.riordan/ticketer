<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_payments_table', function (Blueprint $table) {
            // be very carefully about compromising user security please
            $table->bigIncrements('id');
            $table->string('card_holder_name');
            $table->string('card_number'); // should only store last 4 digits honestly
            // probably don't store anything else
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_payments_table');
    }
}
