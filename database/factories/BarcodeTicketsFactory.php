<?php

use Faker\Generator as Faker;

$factory->define(App\Tickets\BarcodeTicket::class, function (Faker $faker) {
    return [
        'code'       => App\HashGenerator::hash(),
        'first_name' => $faker->firstName,
        'last_name'  => $faker->lastName,
    ];
});
