<?php

use Faker\Generator as Faker;

$classes = [
    App\Events\MusicEvent::class
];

$classNames = [
    'MusicEvent',
];


$attendableAutoIncrement = attendableAutoIncrement();

$factory->define(App\Events\Event::class, function (Faker $faker) use ($attendableAutoIncrement, $classes, $classNames) {
	$attendableAutoIncrement->next();
    return [
        'attendable_id'   => factory($classes[$attendableAutoIncrement->current() % count($classes)])->create()->id,
        'attendable_type' => $classNames[$attendableAutoIncrement->current() % count($classNames)],
        //'ticketable_type' => $ticketClassNames[$attendableAutoIncrement->current() % count($ticketClasses)],
    ];
});

function attendableAutoIncrement()
{
    for ($i = 0; $i < 1000; $i++) {
        yield $i;
    }
}