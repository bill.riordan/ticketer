<?php

use Faker\Generator as Faker;

$classes = [
    App\Tickets\BarcodeTicket::class,
    App\Tickets\QrTicket::class
];

$classNames = [
    'QrTicket',
    'BarcodeTicket'
];

$ordersAutoIncrement = ordersAutoIncrement();
$eventsAutoIncrement = eventsAutoIncrement();
$venuesAutoIncrement = venuesAutoIncrement();

$factory->define(App\Order::class, function (Faker $faker) use ($ordersAutoIncrement, $classes, $classNames, $eventsAutoIncrement, $venuesAutoIncrement)
{
	$ordersAutoIncrement->next(); // advance to next order
	$eventPrice = random_int(500, 5000); // in pennies!
	$surcharge = ((0.29) * $eventPrice) + 130; // Stripe API + $1 for us :)
	//
	// create new Venue(s) object every 5 Event(s)
	//
	if ($ordersAutoIncrement->current() % 25 == 0) {
		$venuesAutoIncrement->next();
		$venue = factory(App\Venue::class)->create();
	}

	//
	// create new Event(s) object every 5 Order(s)
	//
	if ($ordersAutoIncrement->current() % 5 == 0) {
		$start_date   = $faker->dateTimeBetween('-1 year', '+1 years');
		$on_sale_date = $faker->dateTimeBetween('-1 year', $start_date);
		$eventsAutoIncrement->next();
		$event = factory(App\Events\Event::class)->create([
				'ticketable_type'        => $classNames[$eventsAutoIncrement->current() % 2],
				'venue_id'               => $venuesAutoIncrement->current(),
				'general_admission_cost' => $eventPrice,
				'start_date' 			 => $start_date,
				'on_sale_date' 			 => $on_sale_date,
			]);
	}

	//
	// create 5 Ticket(s) (and their ticketables) every 1 Order(s) NULLIFIED
	//
	if ($ordersAutoIncrement->current() % 1 == 2) {
		for ($i = 0; $i < 5; $i++) {
			factory(App\Tickets\Ticket::class)->create([
				'ticketable_id'   => factory($classes[$eventsAutoIncrement->current() % 2])->create()->id,
				'ticketable_type' => $classNames[$eventsAutoIncrement->current() % 2],
				'order_id'        => $ordersAutoIncrement->current(),
			]);
		}
	}

	//
	// create 1 Payment(s) (and their payables) every 1 Order(s) NULLIFIED
	//
	if ($ordersAutoIncrement->current() % 1 == 2) {
		for ($i = 0; $i < 5; $i++) {
			factory(App\Payments\Payment::class)->create([
				'order_id'  => $ordersAutoIncrement->current(),
				'subtotal'  => $eventPrice,
				'surcharge' => $surcharge,
			]);
		}
	}

	//
	// create 1 order
	//
    return [
        'event_id' => $eventsAutoIncrement->current(),
        'hash'	   => App\HashGenerator::hash(),
    ];
});

function ordersAutoIncrement()
{
    for ($i = -1; $i < 1000; $i++) {
        yield $i;
    }
}

function eventsAutoIncrement()
{
    for ($i = 0; $i < 1000; $i++) {
        yield $i;
    }
}

function venuesAutoIncrement()
{
    for ($i = 0; $i < 1000; $i++) {
        yield $i;
    }
}