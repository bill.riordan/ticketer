<?php

use Faker\Generator as Faker;

$factory->define(App\Venue::class, function (Faker $faker) {
	$name = $faker->company;
    return [
        'name'        		   => $name,
        'slug'        		   => App\SlugGenerator::slug($name),
        'description' 		   => $faker->realText(),
        'per_ticket_surcharge' => random_int(1, 20) * 10,
    ];
});
