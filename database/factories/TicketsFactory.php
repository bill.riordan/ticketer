<?php
/*
    This creates base Tickets
    and ticketable_id spawns another factory to 
    create ticetable Tickets
*/
use Faker\Generator as Faker;

$classes = [
    App\Tickets\BarcodeTicket::class,
    App\Tickets\QrTicket::class
];

$classNames = [
    'QrTicket',
    'BarcodeTicket'
];

$ticketsAutoIncrement = ticketsAutoIncrement();

$factory->define(App\Tickets\Ticket::class, function (Faker $faker) use ($ticketsAutoIncrement, $classes, $classNames) {
	$ticketsAutoIncrement->next();
    return [
        //'ticketable_id'   => factory($classes[$ticketsAutoIncrement->current() % count($classes)])->create()->id,
        //'ticketable_type' => $classNames[$ticketsAutoIncrement->current() % count($classNames)],
        'purchased'       => true,
        'scanned'         => false,
        //'event_id'        => $ticketsAutoIncrement->current(),
    ];
});

function ticketsAutoIncrement()
{
    for ($i = 0; $i < 1000; $i++) {
        yield $i;
    }
}