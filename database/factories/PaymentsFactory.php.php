<?php

use Faker\Generator as Faker;

$classes = [
    App\Payments\CreditCardPayment::class
];

$classNames = [
    'CreditCardPayment',
];

$statuses = [
	'pending',
	'success',
	'failed'
];

$paymentsAutoIncrement = paymentsAutoIncrement();

$factory->define(App\Payments\Payment::class, function (Faker $faker) use ($paymentsAutoIncrement, $classes, $classNames, $statuses) {
	$paymentsAutoIncrement->next();
    return [
        'payable_id'   => factory($classes[$paymentsAutoIncrement->current() % count($classes)])->create()->id,
        'payable_type' => $classNames[$paymentsAutoIncrement->current() % count($classNames)],
        //'ticket_id'    => $paymentsAutoIncrement->current(),
        'status'	   => $statuses[$paymentsAutoIncrement->current() % count($statuses)]
    ];
});

function paymentsAutoIncrement()
{
    for ($i = 0; $i < 1000; $i++) {
        yield $i;
    }
}