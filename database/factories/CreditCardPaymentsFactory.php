<?php

use Faker\Generator as Faker;

$factory->define(App\Payments\CreditCardPayment::class, function (Faker $faker) {
    return [
        'card_number'      => sprintf('%16d', random_int(0, 9999999999999999)),
        'card_holder_name' => $faker->name,
    ];
});
