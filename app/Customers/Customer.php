<?php

namespace App\Customers;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function actionable()
    {
        return $this->morphTo();
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    /**
     * create actionable from input
     */
    public function createActionable($request): void
    {
        $guest 		  = new Guest();
        $guest->email = $request->input('email');
        $guest->name  = $request->input('full_name');
        $guest->save();

        // fill customer
        $this->actionable_type = 'Guest';
        $this->actionable_id   = $guest->id;
    }
}
