<?php

namespace App\Customers;

interface ContactInformationInterface
{
	public function getEmail(): string;

	public function getName(): string;
}