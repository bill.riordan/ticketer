<?php

namespace App\Customers;

use Illuminate\Database\Eloquent\Model;

class Guest extends Customer implements ContactInformationInterface
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    protected $table = 'guests';

    protected $morphClass = 'Guest';

    public function customer()
    {
        return $this->morphOne('App\Customers\Customer', 'actionable');
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
