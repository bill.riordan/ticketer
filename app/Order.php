<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Payments\Payment;

class Order extends Model
{

    /**
     *  this will need to be MANY TO MANY for more robust checkout process
     *  but this is a huge refactor job
     */
	public function event()
    {
    	return $this->belongsTo('App\Events\Event');
    }

    public function tickets()
    {
    	return $this->hasMany('App\Tickets\Ticket');
    }

    public function payments()
    {
    	return $this->hasMany('App\Payments\Payment');
    }

    public function customer()
    {
    	return $this->belongsTo('App\Customers\Customer');
    }

    public function getSubtotal()
    {
        // (true subtotal) + (venue's per ticket surcharge)
        return ($this->event->general_admission_cost * $this->tickets->count()) + ($this->tickets->count() * $this->event->venue->per_ticket_surcharge);
    }

    public function formatForResponse(): array
    {
        $response = [];
        $response['order_id'] = $this->hash;
        $response['event_id'] = $this->event_id;
        if ($this->payments !== null) {
            $subtotal = 0;
            $surcharge = 0;
            $status = Payment::SUCCESS;
            foreach ($this->payments as $payment) {
                $subtotal += $payment->subtotal;
                $surcharge += $payment->surcharge;
                // get the worst payment status
                if ($payment->status !== Payment::SUCCESS && $status !== Payment::FAILED) {
                    $status = $payment->status;
                }
            }
            $response['subtotal'] = $subtotal;
            $response['surcharge'] = $surcharge;
            $response['total'] = $response['subtotal'] + $response['surcharge'];
            $response['status'] = $status;
        }
        if ($this->customer !== null) {
            $response['name'] = $this->customer->actionable->getName();
        }
        $response['tickets'] = $this->tickets->count();

        return $response;
    }
}
