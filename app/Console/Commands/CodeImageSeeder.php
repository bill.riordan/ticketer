<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Tickets\Ticket;
use App\Repositories\Tickets\TicketsRepository;
use Illuminate\Filesystem\Filesystem;

class CodeImageSeeder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'image:seeder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create ticket code images after db seed';

    protected $ticketsRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TicketsRepository $ticketsRepository)
    {
        parent::__construct();
        $this->ticketsRepository = $ticketsRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // reset
        \File::deleteDirectory(public_path('qr_codes'));
        \File::makeDirectory(public_path('qr_codes'));

        // do work
        $tickets = $this->ticketsRepository->findAll();
        foreach($tickets as $ticket) {
            $ticket->ticketable->createCode();
        }
    }
}
