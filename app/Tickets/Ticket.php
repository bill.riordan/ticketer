<?php

namespace App\Tickets;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use App\HashGenerator;
use Carbon\Carbon;

class Ticket extends Model
{
    const TYPES = [
        'BarcodeTicket' => BarcodeTicket::class,
        'QrTicket'      => QrTicket::class,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'scanned', 'ticketable_type', 'ticketable_id'
    ];

    public static function getTypes()
    {
        return self::TYPES;
    }

    public function ticketable()
    {
        return $this->morphTo();
    }

    public function isScanned(): bool
    {
        return $this->scanned;
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function payments()
    {
        return $this->hasMany('App\Payments\Payment');
    }

    /**
     * create ticketable from order
     */
    public function createTicketable($order): void
    {
        $types = Ticket::getTypes();
        $ticketable = new $types[$order->event->ticketable_type]();
        $ticketable->code = HashGenerator::hash();
        $ticketable->save();

        // fill ticket
        $this->ticketable_type = $order->event->ticketable_type;
        $this->ticketable_id = $ticketable->id;
        $this->order_id = $order->id;
    }
}
