<?php

namespace App\Tickets;

use Illuminate\Database\Eloquent\Model;
use Endroid\QrCode\QrCode;

class QrTicket extends Ticket implements TicketInterface
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'code'
    ];

    protected $table = 'qr_tickets';

    protected $morphClass = 'QrTicket';

    public function render()
    {
    	return view('qr_ticket')
        ->with([
            'ticket'    => $this,
        ]);
    }

    public function createCode(): void
    {
        $qrCode = new QrCode($this->code);
        $qrCode->writeFile(public_path() . '\\qr_codes\\' . $this->code . '.png');
    }

    public function ticket()
    {
        return $this->morphOne('Ticket', 'ticketable');
    }
}
