<?php

namespace App\Tickets;

use Illuminate\Database\Eloquent\Model;
use Picqer\Barcode\BarcodeGeneratorHTML;

class BarcodeTicket extends Ticket implements TicketInterface
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'code'
    ];

	protected $table = 'barcode_tickets';

	protected $morphClass = 'BarcodeTicket';

    public function render() {
    	$generator = new BarcodeGeneratorHTML();

		return view('barcode_ticket')
        ->with([
            'ticket'    => $this,
            'barcode'	=> $generator->getBarcode($this->code, $generator::TYPE_CODE_128),
        ]);
    }

    public function createCode(): void
    {
		//
    }

    public function ticket()
    {
        return $this->morphOne('Ticket', 'ticketable');
    }
}
