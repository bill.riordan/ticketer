<?php

namespace App\Tickets;

interface TicketInterface
{
	public function render();

	public function createCode(): void;
}