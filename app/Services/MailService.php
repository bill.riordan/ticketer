<?php

namespace App\Services;

use App\Order;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderConfirmation;

class MailService
{
    /**
     * Send Confirmation Email
     */
    public function sendOrderConfirmationEmail(Order $order): void
    {
        Mail::to($order->customer->actionable->getEmail())->send(new OrderConfirmation($order));
    }
}
