<?php

namespace App\Services;

use App\Repositories\Orders\OrdersRepository;
use App\Order;
use App\Payments\Payment;
use App\Customers\Customer;
use App\Customers\Guest;
use App\HashGenerator;

class CheckoutService
{
    private $ordersRepository;

    public function __construct(OrdersRepository $ordersRepository)
    {
        $this->ordersRepository = $ordersRepository;
    }

    /**
     * Create New Order
     */
    public function createOrder($request): Order
    {
        $order = new Order();
        $order->hash = HashGenerator::hash();
        if ($request->input('user_id')) {
            // todo: user id will have to be specified as a hash
            // and will have to hit a UserRepository to get user_id
            // for Relational Mapping
            $user = User::find($request->input('user_id'));
            $order->customer_id = $user->customer->id;
        }
        $order->event_id = $request->input('event_id');
        $order->save();

        return $order;
    }

    /**
     * Do checkout
     */
    public function createGuestAccount($request): Order
    {
        $order = $this->ordersRepository->findOneBy(['hash' => $request->input('order_id')]);
        $customer = new Customer();
        $customer->createActionable($request);
        $customer->save();

        $order->customer_id = $customer->id;
        $order->save();

        return $order;
    }

    /**
     * Do checkout
     */
    public function checkout($request)
    {
        $order = $this->ordersRepository->findOneBy(['hash' => $request->input('order_id')]);

        $paymentDetails = json_decode($request->input('payment_details'), true); // turn this into an array

        // add payments
        $this->preparePayments($order, $paymentDetails);
        // validate
        $errors = $this->validate($order);
        if (!empty($errors)) {
            throw new \Exception(implode(', ', $errors));
        }
        // do payment
        $this->doPayment($order);

        return $order;
    }

    /**
     * Do checkout
     */
    public function getOrder($request): Order
    {
        return $this->ordersRepository->findOneBy(['hash' => $request->input('order_id')]);
    }

    /**
     * Create payables from json --and-- assign amounts to each
     */
    private function preparePayments(Order $order, $paymentDetails): void
    {
        foreach ($paymentDetails as $paymentDetail) {
            $payment = new Payment();
            $payment->createPayable($paymentDetail);
            $payment->order_id = $order->id;
            $payment->save();
        }
        $this->delegatePayments($order);
    }

    /**
     * Return errors list
     */
    private function validate(Order $order): array
    {
        $errors = [];
        if ($order->tickets->isEmpty()) {
            $errors[] = 'there are no tickets in your cart';
        }
        if ($order->payments->isEmpty()) {
            $errors[] = 'please specify a payment method';
        }
        if ($order->customer_id === null) {
            $errors[] = 'nobody owns this order';
        }
        return $errors;
    }

    /**
     * Assign portions of subtotal to payables
     */
    private function delegatePayments(Order $order): void
    {
        $subtotal = $order->getSubtotal();
        foreach ($order->payments as $payment) {
            $allowance = $subtotal - $payment->payable->getAllowance();
            if ($allowance <= 0) {
                // pay the rest of the subtotal off
                $payment->subtotal = $subtotal;
                $payment->surcharge = $payment->payable->calculateSurcharge($subtotal);
                $payment->save();
                break;
            } else {
                // pay the full allowance
                $subtotal -= $allowance;
                $payment->subtotal = $allowance;
                $payment = $payment->payable->calculateSurcharge();
                $payment->surcharge = $payment->payable->calculateSurcharge($subtotal);
                $payment->save();
            }
        }
    }

    /**
     * Process the payments
     */
    private function doPayment(Order $order): bool
    {
        $status = Payment::FAILED;
        foreach ($order->payments as $payment) {
            if ($payment->status !== Payment::PENDING) {
                continue;
            }
            $status = $payment->payable->pay();
            $payment->status = $status;
            $payment->save();
            if ($status == Payment::FAILED) {
                break;
            }
        }

        if ($status == Payment::SUCCESS) {
            return true;
        } else {
            $this->rollback($order);
            return false;
        }
    }

    /**
     * If one of the payments fails, rollback transactions
     * Also set all non-successful payments to FAILED
     */
    private function rollback(Order $order): void
    {
        foreach ($order->payments as $payment) {
            if ($payment->status == Payment::FAILED) {
                $payment->payable->refund();
            }
            if ($payment->status == Payment::PENDING) {
                $payment->status = Payment::FAILED;
                $payment->save();
            }
        }
    }
}
