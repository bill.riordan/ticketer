<?php

namespace App\Services;

use App\Repositories\Events\EventsRepository;
use App\Repositories\Venues\VenuesRepository;
use App\Events\Event;
use App\Events\MusicEvent;
use App\Tickets\Ticket;
use App\HashGenerator;

class EventService
{
    private $eventsRepository;

    private $venuesRepository;

    public function __construct(EventsRepository $eventsRepository, VenuesRepository $venuesRepository)
    {
        $this->eventsRepository = $eventsRepository;
        $this->venuesRepository = $venuesRepository;
    }

    /**
     * Get View Data for EventsController@create
     */
    public function showCreateEvent():array
    {
        $venue = $this->venuesRepository->findOneBy(['slug' => $subdomain]);

        $ticketable_types = Ticket::getTypes();
        $ticketable_types = array_flip($ticketable_types);
        $ticketable_types = array_combine($ticketable_types, $ticketable_types);

        $attendable_types = Event::getTypes();
        $attendable_types = array_flip($attendable_types);
        $attendable_types = array_combine($attendable_types, $attendable_types);

        return [
            'subdomain'        => $subdomain,
            'venue'            => $venue,
            'ticketable_types' => $ticketable_types,
            'attendable_types' => $attendable_types,
        ];
    }

    /**
     * Get View Data for EventsController@edit
     */
    public function showEditEvent($subdomain, $id):array
    {
        $venue = $this->venuesRepository->findOneBy(['slug' => $subdomain]);
        $event = $this->eventsRepository->findOneBy(['id' => $id]);
        
        $ticketable_types = Ticket::getTypes();
        $ticketable_types = array_flip($ticketable_types);
        $ticketable_types = array_combine($ticketable_types, $ticketable_types);

        $attendable_types = Event::getTypes();
        $attendable_types = array_flip($attendable_types);
        $attendable_types = array_combine($attendable_types, $attendable_types);

        return [
            'subdomain'        => $subdomain,
            'venue'            => $venue,
            'ticketable_types' => $ticketable_types,
            'attendable_types' => $attendable_types,
            'event'            => $event,
        ];
    }
    /**
     * Create a single event for this order
     */
    public function createEvent($input, $subodmain): void
    {
        $venue = $this->venuesRepository->findOneBy(['slug' => $subdomain]);
        $event = new Event();
        $event->createAttendable($input, $venue->id);
        $event->save();
    }

    /**
     * Update a single event for this order
     */
    public function updateEvent($input, $id): void
    {
        $event = $this->eventsRepository->findOneBy(['id' => $id]);
        $event->updateAttendable($input);
        $event->save();
    }

    /**
     * Delete a single event for this order
     */
    public function deleteEvent($subdomain, $id): void
    {
        $event = $this->eventsRepository->findOneBy(['id' => $id]);
        $event->delete();
    }
}
