<?php

namespace App\Services;

use App\Repositories\Orders\OrdersRepository;
use App\Tickets\Ticket;
use App\Tickets\QrTicket;
use App\Tickets\BarcodeTicket;
use App\Order;

class TicketService
{
    private $ordersRepository;

    public function __construct(OrdersRepository $ordersRepository)
    {
        $this->ordersRepository = $ordersRepository;
    }
    /**
     * create a single ticket for this order
     * (order HAS ONE event that it creates a single ticket for)
     */
    public function createTickets($request): Order
    {
        $order = $this->ordersRepository->findOneBy(['hash' => $request->input('order_id')]);

        $amount = $request->input('amount');
        for ($i = 0; $i < $amount; $i++) {
            $ticket = new Ticket();
            $ticket->createTicketable($order);
            $ticket->save();
        }

        return $order;
    }

    /**
     * create a single ticket for this order
     * (order HAS ONE event that it creates a single ticket for)
     */
    public function approveTickets(Order $order): void
    {
        foreach ($order->tickets as $ticket) {
            $ticket->purchased = true;
            $ticket->save();
        }
    }
}
