<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Tickets\Ticket;
use App\Tickets\QrTicket;
use App\Tickets\BarcodeTicket;
use App\Repositories\Tickets\TicketsRepository;
use App\Http\Requests\PostTicketScan;

class TicketsController extends Controller
{
	private $ticketRepository;

	public function __construct(TicketsRepository $ticketRepository)
	{
		$this->ticketRepository = $ticketRepository;
	}

    /**
     * Scan Ticket
     * ROUTE('POST', '/api/tickets/scan')
     * body(code)
     */
    public function scan(PostTicketScan $request)
    {
        $ticket = $this->ticketRepository->findOneBy(['code' => $request->input('code')]);
        if ($ticket->purchased && !$ticket->scanned) {
            $ticket->scanned = true;
            $ticket->save();
            return response(true, Response::HTTP_OK);
        }
        return response(false, Response::HTTP_NOT_FOUND);
    }
}
