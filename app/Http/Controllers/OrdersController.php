<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Order;
use App\HashGenerator;
use App\Tickets\Ticket;
use App\Repositories\Orders\OrdersRepository;
use App\Services\CheckoutService;
use App\Services\TicketService;
use App\Services\MailService;
use App\Http\Requests\PostCreateNewOrder;
use App\Http\Requests\PostContactInformation;
use App\Http\Requests\PostTickets;
use App\Http\Requests\PostCheckout;
use App\Http\Requests\GetOrder;
use App\Http\Requests\GetReceipt;

class OrdersController extends Controller
{
    private $checkoutService;

    private $ticketService;

	private $mailService;

	public function __construct(OrdersRepository $ordersRepository)
    {
        $this->checkoutService  = new CheckoutService($ordersRepository);
        $this->ticketService    = new TicketService($ordersRepository);
        $this->mailService      = new MailService();
	}

	/**
	 * Create New Order
     * ROUTE('POST', '/api/orders/create_order')
     * body(event_id, user_id [optional])
	 */
    public function postCreateNewOrder(PostCreateNewOrder $request)
    {
        try {
            $order = $this->checkoutService->createOrder($request);
        } catch (\Throwable $e) {
            return response('order could not be created', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        if ($order === null) {
            return response('failed', Response::HTTP_CONFLICT);
        }

        return response(json_encode($order->formatForResponse()), Response::HTTP_OK);
    }

    /**
     * Contact Information
     * ROUTE('POST', '/api/orders/contact_information')
     * body(order_id, email, full_name)
     */
    public function postContactInformation(PostContactInformation $request)
    {
        try {
            $order = $this->checkoutService->createGuestAccount($request);
        } catch (\Throwable $e) {
            return response('order not found', Response::HTTP_NOT_FOUND);
        }

        if ($order === null) {
            return response('failed', Response::HTTP_CONFLICT);
        }
        
        return response(json_encode($order->formatForResponse()), Response::HTTP_OK);
    }

    /**
     * Add Ticket(s)
     * ROUTE('POST', '/api/orders/add_tickets')
     * body(order_id, amount)
     */
    public function postTickets(PostTickets $request)
    {
        try {
            $order = $this->ticketService->createTickets($request);
        } catch (\Throwable $e) {
            return response('order not found', Response::HTTP_NOT_FOUND);
        }

        if ($order === null) {
            return response('failed', Response::HTTP_CONFLICT);
        }

        return response(json_encode(['success', $request->input('amount') . ' ticket(s) created.', $order->formatForResponse()]), Response::HTTP_OK);
    }

    /**
     * Checkout Order
     * ROUTE('POST', '/api/orders/checkout')
     * body(order_id, payment_details)
     */
    public function postCheckout(PostCheckout $request)
    {
        try {
            $order = $this->checkoutService->checkout($request);
        } catch (\Throwable $e) {
            return response($e, Response::HTTP_NOT_FOUND);
        }
        if ($order !== null) {
            $this->ticketService->approveTickets($order);
            $this->mailService->sendOrderConfirmationEmail($order);
            return response(json_encode($order->formatForResponse()), Response::HTTP_OK);
        }
        return response(json_encode($order->formatForResponse()), Response::HTTP_PAYMENT_REQUIRED);
    }

    /**
     * Show Order
     * ROUTE('GET', '/api/orders/show_order')
     * params(order_id)
     */
    public function getOrder(GetOrder $request)
    {
        try {
            $order = $this->checkoutService->getOrder($request);
        } catch (\Throwable $e) {
            return response('order not found', Response::HTTP_NOT_FOUND);
        }

        return response(json_encode($order->formatForResponse()), Response::HTTP_OK);
    }
}
