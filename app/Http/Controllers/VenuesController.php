<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Events\EventsRepository;
use App\Repositories\Venues\VenuesRepository;
use App\Services\VenueService;

class VenuesController extends Controller
{
    private $eventsRepository;

	private $venuesRepository;

	private $venueService;

	public function __construct(EventsRepository $eventsRepository, VenuesRepository $venuesRepository)
	{
        $this->eventsRepository = $eventsRepository;
        $this->venuesRepository = $venuesRepository;
        $this->venueService     = new VenueService();
	}

	/**
     * deprecated (will be handled in backstage)
     * Show all Venues
     */
    public function index($subdomain)
    {
    	$venues = $this->venuesRepository->findAll();
    }

	/**
     * Show Single Venue Dashboard
     */
    public function show($subdomain)
    {
    	$venue = $this->venuesRepository->findOneBy(['slug' => $subdomain]);

        // for events partials
        $futureEvents = $this->eventsRepository->findFutureEventsBy(['venue_id' => $venue->id]);
        $pastEvents = $this->eventsRepository->findPastEventsBy(['venue_id' => $venue->id]);

        //for stats partials
        $totalRevenue = 0;
        $totalTickets = 0;
        foreach ($venue->events as $event) {
            $totalRevenue += $event->general_admission_cost * $event->tickets->count();
            $totalTickets += $event->tickets->count();
        }
        
    	return view('venues.show')
    	->with([
    		'venue'        => $venue,
            'futureEvents' => $futureEvents,
            'pastEvents'   => $pastEvents,
            'totalRevenue' => $totalRevenue,
            'totalTickets' => $totalTickets,
    	]);
    }

    /**
     * deprecated (will be handled in backstage)
	 * Create New Subdomain
	 */
    public function create()
    {
    	$venue = $this->venueService->createVenue();
    	return redirect('/');
    }

	/**
	 * Create New Subdomain
	 */
    public function update($subdomain)
    {
    	$venue = $this->venuesRepository->findOneBy(['slug' => $subdomain]);
    	$venue = $this->venueService->updateVenue();
    	return redirect('/');
    }

    /**
     * deprecated (will be handled in backstage)
	 * Create New Subdomain
	 */
    public function delete()
    {
    	$venue = $this->venueService->deleteVenue();
    }
}
