<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Events\Event;
use App\Repositories\Events\EventsRepository;
use App\Repositories\Venues\VenuesRepository;
use App\Services\EventService;
use App\Http\Requests\PostCreateNewEvent;

class EventsController extends Controller
{
	private $eventService;

	public function __construct(EventsRepository $eventsRepository, VenuesRepository $venuesRepository)
	{
        $this->eventService     = new EventService($eventsRepository, $venuesRepository);
	}

    public function show()
    {
    	//
    }

    /**
     * Create an Event for a specific Venue
     */
    public function create($subdomain)
    {
        $viewData = $this->eventService->showCreateEvent($subdomain);

    	return view('events.create')->with($viewData);
    }

    /**
     * Store new Event
     */
    public function store($subdomain)
    {
    	$status = $this->eventService->createEvent(Input::all(), $subdomain);

        return redirect('/');
    }

    /**
     * Edit an Event for a specific Venue
     */
    public function edit($subdomain, $id)
    {
        $viewData = $this->eventService->showEditEvent($subdomain, $id);

        return view('events.edit')->with($viewData);
    }

    /**
     * Update existing Event
     */
    public function update($subdomain, $id)
    {
        $status = $this->eventService->updateEvent(Input::all(), $id);

        return redirect('/');
    }

    /**
     * Delete an Event for a specific Venue
     */
    public function delete($subdomain, $id)
    {
    	$this->eventService->deleteEvent($id);

        return redirect('/');
    }
}
