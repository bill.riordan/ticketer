<?php

namespace App\Payments;

interface PaymentInterface
{
	public function getAllowance(): int;

	public function pay(): string;

	public function refund(): bool;

	public function calculateSurcharge($subtotal): int;
}