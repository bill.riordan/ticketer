<?php

namespace App\Payments;

use Illuminate\Database\Eloquent\Model;
use App\Payments\Payment;

class CreditCardPayment extends Payment implements PaymentInterface
{
    const CREDIT_ALLOWANCE = 99999999;
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'card_holder_name', 'card_number'
    ];

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];
    
	protected $table = 'credit_payments_table';

    protected $morphClass = 'CreditCardPayment';

    public function getAllowance(): int
    {
        return self::CREDIT_ALLOWANCE;
    }
    
    public function pay(): string
    {
        // this will be the stripe API hit
        return parent::SUCCESS;
    }

    public function refund(): bool
    {
        // this will be the stripe API hit
        return true;
    }

    /**
     * Validate payment and add a surcharge
     */
    public function calculateSurcharge($subtotal): int
    {
        //$surcharge = ((0.029) * $subtotal) + 30); // Stripe API
        $subcharge = ($subtotal - 30) / (0.931);
        $surcharge = $subcharge - $subtotal;
        return ceil($surcharge);
    }
}
