<?php

namespace App\Payments;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
	const SUCCESS = 'success';
	const PENDING = 'pending';
	const FAILED = 'failed';

	public function payable()
    {
        return $this->morphTo();
    }

    public function order()
    {
    	return $this->belongsTo('App\Order');
    }

    /**
     * create payables from json
     * (this will create duplicates because it's dumb)
     */
    public function createPayable($paymentRequest): void
    {
        // create payable obj
        $payable = new $paymentRequest['payment_type']();
        $payable->fill($paymentRequest); // HUGE security concern here
        $payable->save();

        // update Payment
        $this->payable_id = $payable->id;
        $this->payable_type = $paymentRequest['payment_type'];
    }
}
