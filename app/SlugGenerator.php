<?php

namespace App;

class SlugGenerator
{
    public static function slug($string)
    {
    	$preparedString = preg_replace('/[^a-z\s*\-*]/', '', strtolower($string));
        return preg_replace('/[\s*]/', '-', $preparedString);
    }
}
