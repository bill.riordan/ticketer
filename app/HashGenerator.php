<?php

namespace App;

use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class HashGenerator
{
    public static function hash()
    {
        return hash('md5', Carbon::now()->format('l jS \\of F Y h:i:s A') . random_int(PHP_INT_MIN, PHP_INT_MAX));
    }
}
