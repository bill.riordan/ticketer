<?php

namespace App\Events;

use Illuminate\Database\Eloquent\Model;
use App\Events\MusicEvent;

class Event extends Model
{
    const TYPES = [
        'MusicEvent' => MusicEvent::class,
    ];

    public static function getTypes()
    {
        return self::TYPES;
    }

    public function attendable()
    {
        return $this->morphTo();
    }

    public function venue()
    {
    	return $this->belongsTo('App\Venue');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function tickets()
    {
    	return $this->hasManyThrough('App\Tickets\Ticket', 'App\Order');
    }

    /**
     * Create attendable from input
     */
    public function createAttendable($input, $venue_id): void
    {
        $attendable              = new $input['attendable_type']();
        $attendable->name        = $input['name'];
        $attendable->description = $input['description'];
        $attendable->save();

        // fill event
        $this->attendable_id          = $attendable->id;
        $this->attendable_type        = $input['attendable_type'];
        $this->ticketable_type        = $input['ticketable_type'];
        $this->general_admission_cost = $input['general_admission_cost'] * 100;
        $this->on_sale_date           = $input['on_sale_date'];
        $this->venue_id               = $venue_id;
    }

    /**
     * Create attendable from input
     */
    public function updateAttendable($input): void
    {
        $attendable              = $this->attendable;
        $attendable->name        = $input['name'];
        $attendable->description = $input['description'];
        $attendable->save();

        // fill event
        $this->attendable_id          = $attendable->id;
        $this->attendable_type        = $input['attendable_type'];
        $this->ticketable_type        = $input['ticketable_type'];
        $this->general_admission_cost = $input['general_admission_cost'] * 100;
        $this->on_sale_date           = $input['on_sale_date'];
    }
}
