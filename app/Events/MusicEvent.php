<?php

namespace App\Events;

use Illuminate\Database\Eloquent\Model;

class MusicEvent extends Model
{
    public function event()
    {
        return $this->morphOne('Event', 'attendable');
    }
}
