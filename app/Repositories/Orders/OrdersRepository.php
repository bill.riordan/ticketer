<?php

namespace App\Repositories\Orders;

use App\Repositories\RepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Order;
use DB;

class OrdersRepository implements RepositoryInterface
{
	public function findAll(): Collection
	{
		return Order::all();
	}

	public function findAllBy(array $searchTerm): Collection
	{
		//
	}

	public function findOneBy(array $searchTerm): Model
	{
		return Order::where($searchTerm)->get()->first();
	}
}