<?php

namespace App\Repositories\Events;

use App\Repositories\RepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Events\Event;
use Carbon\Carbon;
use DB;

class EventsRepository implements RepositoryInterface
{
	public function findAll(): Collection
	{
		return Event::all();
	}

	public function findAllBy(array $searchTerm): Collection
	{
		//
	}

	public function findOneBy(array $searchTerm): Model
	{
		return Event::where($searchTerm)->get()->first();
	}

	public function findFutureEventsBy(array $searchTerm): Collection
	{
		return Event::where($searchTerm)->where('start_date', '>', Carbon::now()->subDays(1))->orderBy('start_date', 'asc')->get();
	}

	public function findPastEventsBy(array $searchTerm): Collection
	{
		return Event::where($searchTerm)->where('start_date', '<', Carbon::now()->subDays(1))->orderBy('start_date', 'desc')->get();
	}
}