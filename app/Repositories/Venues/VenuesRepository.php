<?php

namespace App\Repositories\Venues;

use App\Repositories\RepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Venue;
use DB;

class VenuesRepository implements RepositoryInterface
{
	public function findAll(): Collection
	{
		return Venue::all();
	}

	public function findAllBy(array $searchTerm): Collection
	{
		//
	}

	public function findOneBy(array $searchTerm): Model
	{
		return Venue::where($searchTerm)->get()->first();
	}
}