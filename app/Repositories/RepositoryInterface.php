<?php

namespace App\Repositories;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
	public function findAll(): Collection;

	public function findAllBy(array $searchTerm): Collection;

	public function findOneBy(array $searchTerm): Model;
}