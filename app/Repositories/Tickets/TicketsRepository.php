<?php

namespace App\Repositories\Tickets;

use App\Repositories\RepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Tickets\Ticket;
use DB;

class TicketsRepository implements RepositoryInterface
{
	public function findAll(): Collection
	{
		return Ticket::all();
	}

	public function findAllBy(array $searchTerm): Collection
	{
		//
	}

	public function findOneBy(array $searchTerm): Model
	{
		return Ticket::where($searchTerm)->get()->first();
	}
}