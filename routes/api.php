<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Orders
Route::post('/orders/create_order', 'OrdersController@postCreateNewOrder');
Route::post('/orders/contact_information', 'OrdersController@postContactInformation');
Route::post('/orders/add_tickets', 'OrdersController@postTickets');
Route::post('/orders/checkout', 'OrdersController@postCheckout');
Route::get('/orders/show_order', 'OrdersController@getOrder');
Route::get('/orders/receipt', 'OrdersController@getReceipt');

// Tickets
Route::get('/tickets/scan', 'TicketsController@scan');

// Events
Route::post('/events/create_event', 'EventsController@postCreateNewEvent');