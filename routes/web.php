<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::domain('{subdomain}.' . env('APP_BASE_DOMAIN', 'localhost'))->group(function () {
    Route::get('/', 'VenuesController@show');
    Route::get('/events/add', 'EventsController@create');
    Route::post('/events/store', 'EventsController@store')->name('events.store');
    Route::get('/events/{id}/edit', 'EventsController@edit');
    Route::post('/events/{id}/update', 'EventsController@update')->name('events.update');
    Route::get('/events/{id}/delete', 'EventsController@delete');
});
